| port | application  | remarks                 |
| ---- | ------------ | ----------------------- |
| 9200 | elastisearch | all api calls over http |
| 9300 | elastisearch | internode communication |
