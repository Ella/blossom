#### Useful Docs:
- https://mindchasers.com/dev/netfilter

#### Non-root port forwards
*Note* : Non root users cannot bind ports below 1024
##### Workarounds :

- We can provide the cap_net_bind_service capibility to the executable
- We can reroute traffic from below ports to a port we have control off.

###### Reroute Traffic using iptables
forward the port as *root*
```bash
iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 8080
```

in order to use localhost:

*as root*: as loopback devices (like localhost) do not use the pre-routing rules

```bash
iptables -t nat -I OUTPUT -p tcp -d 127.0.0.1 --dport 80 -j REDIRECT --to-ports 8080
```

**NOTE** : Be careful as the any user can a high number port *(like 8080 in the above example)* and intercept the traffic being redirected to it. 