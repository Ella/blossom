
### SSH Tunneling
---

##### Port Foward
```bash
ssh [-i <filename>] -NfL [bindaddress:]port:endpoint:endpoint_port remote_user@remote
```
- `bindaddress` : which ip to bind the listening port to
- `port` : listening port number
- `endpoint` : url / ip where to push the connection packets to
- `endpoint_port` : port number to push the connection to
---

##### Dynamic Port Forward
```bash
ssh [-i <filename>] -NfD [bindaddress:]port remote_user@remote
```
- `bindaddress` : which ip to bind the listening port to
- `port` : listening port number

**NOTE** : Dynamic Port forward sets up a socks5 proxy. We can use this using protocol:
- `socks5://` : to only forward connection packets 
- `socks5h://` : to resolve URL from perspective of ssh recipient and to push packets there
---

##### Reverse Port Forward
```bash
ssh [-i <filename>] -NfR [remote_listen_ip:]remote_port:endpoint:endpoint_port remote_user@remote
```
- `remote_listen_ip` : on which ip address should remote listen from
- `remote_port` : port number of which remote will listen to
- `endpoint` : url / ip where to push the connection packets to. Can be `localhost`
- `endpoint_port` : port number to push the connection to