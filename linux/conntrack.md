- Conntrack tracks the connections in and out of the system. 
- Useful for NAT and firewalls
- Conntrack tables have a max number of entries that can be entered

#### Useful Docs:
- https://blog.cloudflare.com/conntrack-tales-one-thousand-and-one-flows/
- https://deploy.live/blog/kubernetes-networking-problems-due-to-the-conntrack/
- https://www.tigera.io/blog/when-linux-conntrack-is-no-longer-your-friend/
- https://arthurchiao.art/blog/conntrack-design-and-implementation/

General Formula for Conntrack Max Connections:
```
CONNTRACK_MAX = RAMSIZE (in bytes) / 16384 / (x / 32) where x is the number of bits in a pointer (for example, 32 or 64 bits)
```


#### Conntrack Configs
Check conntrack table max entries: 
```bash
sysctl net.netfilter.nf_conntrack_max
```
Modify Conntrack Max Connection Count: 
```bash
sysctl -w net.netfilter.nf_conntrack_max=393216
```
Modify Conntrack Permanently:
```bash
echo "net.netfilter.nf_conntrack_max = 393216" > /etc/sysctl.d/conntrack.conf
sysctl -p
```
**NOTE** : After setting conntrack.conf above, the change only will take hold after a reboot as the files are loaded on boot. To take affect immeditely we also need to run `sysctl -w` as well

#### Debug Conntrack Connection Issues

Export conntrack table:
```bash
conntrack -L > table.log
```

Show Entries by Source IP Address for tcp:
```bash
grep -v "ucp" table.log | awk "{print $5}" | sort | uniq -c | sort
```

Show Entries by Destination IP for tcp:
```bash
grep -v "ucp" table.log | awk "{print $6}" | sort | uniq -c | sort
```

Show Entries by Source Port by IP for tcp:
```bash
grep -v "ucp" table.log | awk "{print $7}" | sort | uniq -c | sort
```

Show Entries by Source Port by IP for tcp:
```bash
grep -v "ucp" table.log | awk "{print $8}" | sort | uniq -c | sort
```