#### Useful Docs:
- https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#never-co-located-in-the-same-node
- https://kubernetes.io/docs/tutorials/stateful-application/zookeeper/#tolerating-node-failure