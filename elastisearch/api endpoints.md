
##### Get Cluster Status 
```HTTP
GET /_cluster/health
```


## Snapshots
---
##### List Snapshots
```HTTP
GET /_snapshot
```

##### Describe Snapshots
```HTTP
GET /_snapshot/mysnapshot
```

##### Manually Verify Snapshot repositories
```HTTP
POST /_snapshot/mysnapshot/_verify
```


### Nodes
---
List nodes and master
```HTTP
GET /_cat/nodes?v=true
```


### Indices
---

##### Sort Indices by their size:
```HTTP
GET _cat/indices?v&s=store.size:desc
```
